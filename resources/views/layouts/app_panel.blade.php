<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('partials.panel_htmlheader')

<body class="animated fadeIn">
	<div class="wrapper">
    	@yield('content')
    </div>

</body>
</html>
