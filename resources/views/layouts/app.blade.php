<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('partials.index_htmlheader')

<body class="animated fadeIn">
    @yield('content')
</body>
</html>
