    <footer class="contenedor_footer wow fadeInUp">
        <div class="container wow fadeInUp">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 col-lg-4 text-center text-md-left">
                    <h3 class="titulo_footer text-center text-md-left">BLog</h3>
                    <div class="contenedor_blog">
                        @foreach($blogs as $blog)
                            <div class="contenedor_post">
                                <a href="/Blog/{{$blog->id}}" class="post_footer">{{$blog->titulo}}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-center mt-5 mt-md-0">
                    <a href="index.html" class="contenedor_logo_footer"><img src="/{{$footer->logo}}" alt="logo sg3 footer"></a>
                </div>
                <div class="col-12 col-md-6 col-lg-4 text-center text-md-left mt-5 mt-md-5 mt-lg-0">
                    <h3 class="titulo_footer text-center text-md-left">{{$footer->titulo}}</h3>
                    <div class="contenedor_telefonos">
                        <a href="#"><i class="fas fa-phone"></i> {{$footer->telef_f}}</a>
                        <a href="#"><i class="fas fa-mobile-alt"></i> {{$footer->telef_c}}</a>
                        <a href="#"><i class="fas fa-map-marker"></i> {{$footer->ubica}}</a>
                        <a href="#"><i class="fab fa-facebook-f"></i> {{$footer->fb}}</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- copy -->
    <div class="contenedor_copy">
        Copyright© 2018 <strong>SG3 PERÚ</strong> - Diseñado y Desarrollado por <a href="https://creatos.pe" target="_blank">CREATOS</a>
    </div>
    
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/ekko-lightbox.min.js"></script>
    <script src="/js/smooth-scroll.min.js"></script>
    <script src="/js/wow.min.js"></script>
    <script src="/js/script.js"></script>
    <script>
        var scroll = new SmoothScroll('a[href*="#"]');
    </script>