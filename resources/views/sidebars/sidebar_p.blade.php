<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">


      <div class="logo">
        <a href="/SG3_Panel" class="simple-text logo-normal">
         <img src="/img/logo-02.svg" alt="Logo Sg3" style="width: 30%"> SG3 Perú
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active  ">
            <a class="nav-link" href="/SG3_Panel">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="/SG3_Usuarios">
              <i class="material-icons">person</i>
              <p>Usuarios</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="/SG3_Config">
              <i class="material-icons">build</i>
              <p>Configuración</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="/SG3_Contactos">
              <i class="material-icons">location_ons</i>
              <p>Contactos</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="#">
              <i class="material-icons">bubble_chart</i>
              <p>Blog</p>
            </a>
          </li>          
          <li class="nav-item ">
            <a class="nav-link" href="#">
              <i class="material-icons">code</i>
              <p>Soporte al Usuario</p>
            </a>
          </li>
        </ul>
      </div>
    </div>