<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Sg3 Perú</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <!-- fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <!-- favicon -->
    <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Audiowide" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <!-- Menu Animado -->
    <link rel="stylesheet" href="/css/hamburgers.min.css">

    <!-- LightBox -->
    <link rel="stylesheet" href="/css/ekko-lightbox.css">

    <!-- aminete CSS -->
    <link rel="stylesheet" href="/css/animate.min.css">

    <!-- Estilos -->
    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>