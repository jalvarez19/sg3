@extends('layouts.app')

@section('content')

	@include('headers.header_i')

    <!-- SLIDER -->
    @if($errors->any())
        
        <div id="myModal" class="modal">
            <div class="modal-content" style="text-align: center">
                <span class="close" onclick="desparecer()" style="float: right;">&times;</span>
                <strong class="respuesta">Solicitud enviada, </strong><span class="mensaje-alerta">Estaremos en contacto</span>
            </div>                      
        </div>
    @endif 
        
<style>
    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 300px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }

    /* The Close Button */
    .close {
        color: #000;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
</style>
<script>
    function desparecer(){
        document.getElementById('myModal').style.display = 'none';
    }
</script>
    <div id="slide_01_home" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators slide_01">
            <li data-target="#slide_01_home" data-slide-to="0" class="active"></li>
            <li data-target="#slide_01_home" data-slide-to="1"></li>
            <li data-target="#slide_01_home" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="img_slide_01" src="/{{$slide1->image}}" alt="First slide">
                <section class="carousel-caption animated fadeInLeft">
                    <h1 class="titular_slider"><strong>{{$slide1->titulo1}}</strong><br>{{$slide1->titulo2}}</h1>
                    <p class="texto_slider">{{$slide1->texto}}</p>
                </section>
            </div>
            <div class="carousel-item">
                <img class="img_slide_01" src="/{{$slide2->image}}" alt="Second slide">
                <section class="carousel-caption animated fadeInUp">
                    <h1 class="titular_slider"><strong>{{$slide2->titulo1}}</strong><br>{{$slide2->titulo2}}</h1>
                    <p class="texto_slider">{{$slide2->texto}}</p>
                </section>
            </div>
            <div class="carousel-item">
                <img class="img_slide_01" src="/{{$slide3->image}}" alt="Third slide">
                <section class="carousel-caption animated fadeInRight">
                    <h1 class="titular_slider"><strong>{{$slide3->titulo1}}</strong><br>{{$slide3->titulo2}}</h1>
                    <p class="texto_slider">{{$slide3->texto}}</p>
                </section>
            </div>
        </div>
    </div>
    <a class="flecha_abajo" href="#ancla_01"><img src="img/flecha_abajo.svg" alt="Flecha sg3"></a>


    <div id="ancla_01"></div>

    <!-- ESPECIALISTAS -->
    <div class="contenedor_especialistas">
        <div class="container">
            <div class="row justify-content-center">
                <section class="col-12 col-md-auto text-center">
                    <h1 class="titulos_01 wow fadeInLeft">{{$prd03->titulo}}</h1>
                </section>
                <div class="col-12 text-center wow fadeInUp">
                    <p class="texto_01">{{$prd03->texto}}</p>
                </div>

                <div class="col-auto wow fadeInLeft">
                    <div class="contenedor_informacion_especialistas">
                        <img src="/{{$prd01->image}}" alt="seguridad y vigilancia" class="img_especialistas">
                        <h2 class="subtitulo_01">{{$prd01->titulo}}</h2>
                        <p class="texto_02">{{$prd01->texto}}</p>
                        <a href="{{$prd01->link}}" class="boton_01"><i class="fas fa-plus"></i> Más información</a>
                    </div>
                </div>

                <div class="col-auto mt-5 mt-md-0 wow fadeInRight">
                    <div class="contenedor_informacion_especialistas">
                        <img src="/{{$prd02->image}}" alt="seguridad y vigilancia" class="img_especialistas">
                        <h2 class="subtitulo_01">{{$prd02->titulo}}</h2>
                        <p class="texto_02">{{$prd02->texto}}</p>
                        <a href="{{$prd02->link}}" class="boton_01"><i class="fas fa-plus"></i> Más información</a>
                    </div>
                </div>

                <div class="col-12 col-md-auto text-center mt-5">
                    <div class="titulos_01 mt-3 wow fadeInUp">{{$mod03->titulo}}</div>
                </div>

                <div class="col-12 mt-5 mt-md-0">
                    <div class="row justify-content-center">
                        @foreach($certificados as $certificado)
                            <div class="col-auto">
                                <img class="certificaciones_01 wow fadeInLeft" src="/{{$certificado->image}}" alt="mega certificaciones">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- LLAMADA A BLOG -->
    <div class="contenedor_llamada">
        <div class="row justify-content-center">
            <div class="col-12 col-md-auto text-center">
                <h2 class="titulo_blanco wow fadeInUp">{{$mod04->titulo}}</h2>
            </div>
            <div class="col-12 text-center">
                <p class="texto_blanco wow fadeInUp">{{$mod04->texto}}</p>
                <a class="boton_02" href="{{$mod04->link}}">Ir al Blog</a>
            </div>
        </div>
    </div>

    <!-- ESPECIALIDADES -->
    <div class="contenedor_especialidades">
        <div class="container">
            <div class="row justify-content-center">
                <section class="col-12 col-md-auto text-center wow fadeInUp">
                    <h1 class="titulos_01">{{$mod05->titulo}}</h1>
                </section>
            </div>
            <div class="row justify-content-center">
                @foreach($especialidades as $especialidad)
                <div class="col-auto mt-5">
                    <div class="contenido_especialidades wow fadeInUp">
                        <img src="/{{$especialidad->image}}" alt="confianza sg3" class="img_especialidades">
                        <h3 class="titulo_especialidad">{{$especialidad->titulo}}</h3>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- FORMULARIO _01 -->
    <div class="contenedor_llamada">
        <div class="row justify-content-center">
            <div class="col-12 col-md-auto text-center">
                <h2 class="titulo_blanco wow fadeInLeft">FORMA PARTE DE NOSOTROS</h2>
            </div>
            <div class="col-12 text-center wow fadeInLeft">
                <p class="texto_blanco">Quieres formar parte de la familia de SG3 Perú llena tus datos y nos comunicarémos contigo</p>
            </div>
        </div>

        <!-- formulario de contancto -->
        <div class="contenedor_formulario mt-3 wow fadeInLeft">
            <div class="container">
                <form class="formulario-contacto" action="/Formulario_Trabajo" method="POST" role="form">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ingrese su nombre" requerid>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="telefono" id="telefono" placeholder="Ingrese su teléfono" requerid>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="correo" id="correo" placeholder="Ingrese su correo" requerid>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" mensaje="requerid" minlength="20" maxlength="200" name="mensaje" id="mensaje" cols="30" rows="10" placeholder="Escriba su mensaje..."></textarea>
                    </div>
                    
                    <div class="form-group">
                        <button type="submit" class="boton_02">Enviar</button>
                    </div>

                    <div id="alerta" class="alert d-none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>                    
                </form>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    @include('footers.footer_i')
   
@endsection