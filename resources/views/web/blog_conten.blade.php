@extends('layouts.app')

@section('content')

	@include('headers.header_b')
    <script src="/ckeditor/ckeditor.js"></script>
	<div class="contenedor_secciones"></div>

    <!-- NOTICIAS -->
    <div class="contenedor_especialistas">
        <div class="container">
        <section class="col-12 col-md-auto text-center" style="margin-bottom: 5%; width: fit-content; margin-left: auto; margin-right: auto;">
            <h1 class="titulos_01 wow fadeInLeft">Blog informativo y de noticias</h1>
        </section>
            <div class="row justify-content-center align-items-center">                
                <div class="col-12 col-lg-5">
                    <div class="contenedor_img_blog">
                        <img src="/{{$blog->image}}" alt="">
                    </div>
                </div>
                <div class="col-12 col-lg-7 mt-5 mt-lg-0 text-center text-lg-left">
                    <h3 class="titulos_blog">{{$blog->titulo}}</h3>
                    <textarea class="form-control" name="editor1" id="editor1" rows="10" cols="90">{{$blog->texto}}</textarea>            
                </div>
                <div class="col-12 comentarios_noticas mt-5">
                    <form class="comentairos_blog" method="POST" role="form">
                        <div class="form-group">
                            <textarea class="form-control" mensaje="requerid" minlength="20" maxlength="300" name="mensaje" id="mensaje" cols="30" rows="30" placeholder="Escribir Comentario..."></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="boton_02">Comentar</button>
                        </div>
                    </form>
                </div>
                <script>
                    CKEDITOR.replace( 'editor1' );        
                </script>
            </div>
        </div>
    </div>
    <style>
        #cke_1_top{
            display: none;
        }
        #cke_1_bottom{
            display: none;
        }
        .cke_reset{           
            background: none;
        }
        .cke_editable{
            background: none;
            font-size: 15px !important;
        }
        .cke_contents {
            height: 400px !important;
        }
        .align-items-center{
            align-items: start!important;
        }
    </style>
    
    <!-- FOOTER -->
    @include('footers.footer_i')
    
@endsection