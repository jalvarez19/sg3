@extends('layouts.app')

@section('content')

	@include('headers.header_c')
	<div class="contenedor_secciones"></div>

    <!-- FORMULARIO _01 -->
    <div class="contenedor_llamada">
        <div class="row justify-content-center">
            <div class="col-12 col-md-auto text-center">
                <h2 class="titulo_blanco wow fadeInLeft">ESCRÍBENOS Y NOS COMUNICAREMOS CONTIGO</h2>
            </div>
            <div class="col-12 text-center wow fadeInLeft">
                <p class="texto_blanco">Escríbenos y nos pondremos en contacto contigo a la brevedad para poder brindarte el mejor servicio de SEGURIDAD</p>
            </div>
        </div>

        <!-- formulario de contancto -->
        <div class="contenedor_formulario mt-3 wow fadeInLeft">
            <div class="container">
                <form class="formulario-contacto" action="js/enviar.php" method="POST" role="form">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ingrese su nombre" requerid>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="telefono" id="telefono" placeholder="Ingrese su teléfono" requerid>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="correo" id="correo" placeholder="Ingrese su correo" requerid>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" mensaje="requerid" minlength="20" maxlength="200" name="mensaje" id="mensaje" cols="30" rows="10" placeholder="Escriba su mensaje..."></textarea>
                    </div>
                    
                    <div class="form-group">
                        <button type="submit" class="boton_02">Enviar</button>
                    </div>

                    <div id="alerta" class="alert d-none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong class="respuesta"></strong><span class="mensaje-alerta"></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!-- FOOTER -->
    @include('footers.footer_i')
    
@endsection