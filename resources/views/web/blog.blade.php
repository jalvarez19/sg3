@extends('layouts.app')

@section('content')

	@include('headers.header_b')
    <script src="/ckeditor/ckeditor.js"></script>
	<div class="contenedor_secciones"></div>

    <!-- NOTICIAS -->
    <div class="contenedor_especialistas">
        <div class="container">
        <section class="col-12 col-md-auto text-center" style="margin-bottom: 5%; width: fit-content; margin-left: auto; margin-right: auto;">
            <h1 class="titulos_01 wow fadeInLeft">Blog informativo y de noticias</h1>
        </section>
            <dir style="display: none">
                {!!$cont = 0!!}
            </dir>            
            @foreach($blogs as $blog)
            <div class="row justify-content-center align-items-center" style="margin-bottom: 7%">
                <div class="col-12 col-lg-5">
                    <div class="contenedor_img_blog" style="height: 250px">
                        <img src="/{{$blog->image}}" alt="">
                    </div>
                </div>
                <div class="col-12 col-lg-7 mt-5 mt-lg-0 text-center text-lg-left">
                    <h3 class="titulos_blog">{{$blog->titulo}}</h3>
                    <textarea class="form-control" name="editor{{$cont}}" id="editor{{$cont}}" rows="10" cols="90">{{$blog->texto}}</textarea>      
                    <a href="/Blog/{{$blog->id}}"><button type="submit" class="btn btn-danger pull-center">Ver más</button>   </a>
                </div>   
            </div>
            <script>
                CKEDITOR.replace( 'editor{{$cont}}' );        
            </script>
            <dir style="display: none">
                {!!$cont = $cont+1!!}
            </dir>             
            @endforeach
        </div>
    </div>
    
    <style>
        #cke_1_top{
            display: none;
        }
        #cke_1_bottom{
            display: none;
        }
        .cke_reset{           
            pointer-events: none; 
            background: none;
        }
        .cke_editable{
            background: none;
        }
    </style>
    <!-- FOOTER -->
    @include('footers.footer_i')
    
@endsection