@extends('layouts.app')

@section('content')

	@include('headers.header_n')
    @if($errors->any())
        
        <div id="myModal" class="modal">
            <div class="modal-content" style="text-align: center">
                <span class="close" onclick="desparecer()" style="float: right;">&times;</span>
                <strong class="respuesta">Solicitud enviada, </strong><span class="mensaje-alerta">Estaremos en contacto</span>
            </div>                      
        </div>
    @endif 
        
<style>
    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 300px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }

    /* The Close Button */
    .close {
        color: #000;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
</style>
<script>
    function desparecer(){
        document.getElementById('myModal').style.display = 'none';
    }
</script>
	<div class="contenedor_secciones"></div>

    <!-- ESPECIALISTAS -->
    <div class="contenedor_especialistas">
        <div class="container">
            <div class="row justify-content-center">
                <section class="col-12 col-md-auto text-center">
                    <h1 class="titulos_01 wow fadeInLeft">{{$prd03->titulo}}</h1>
                </section>
                <div class="col-12 text-center wow fadeInUp">
                    <p class="texto_01"><strong>{{$prd03->head}}</strong> {{$prd03->texto}}</p>
                </div>

                <div class="col-auto wow fadeInLeft">
                    <div class="contenedor_informacion_especialistas">
                        <img src="/{{$prd01->image}}" alt="misión sg3" class="img_especialistas">
                        <h2 class="subtitulo_01">{{$prd01->titulo}}</h2>
                        <p class="texto_02">{{$prd01->texto}}.</p>
                    </div>
                </div>

                <div class="col-auto mt-5 mt-md-0 wow fadeInRight">
                    <div class="contenedor_informacion_especialistas">
                        <img src="/{{$prd02->image}}" alt="visión sg3" class="img_especialistas">
                        <h2 class="subtitulo_01">{{$prd02->titulo}}</h2>
                        <p class="texto_02">{{$prd02->texto}}</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- FORMULARIO _01 -->
    <div class="contenedor_llamada">
        <div class="row justify-content-center">
            <div class="col-12 col-md-auto text-center">
                <h2 class="titulo_blanco wow fadeInLeft">¿QUIÉRES TENER TU EMPRESA SEGURA?</h2>
            </div>
            <div class="col-12 text-center wow fadeInLeft">
                <p class="texto_blanco">Escríbenos y nos pondremos en contacto contigo a la brevedad para poder brindarte el mejor servicio de SEGURIDAD</p>
            </div>
        </div>

        <!-- formulario de contancto -->
        <div class="contenedor_formulario mt-3 wow fadeInLeft">
            <div class="container">
                <form class="formulario-contacto" action="/Formulario_Empresa" method="POST" role="form">
                @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ingrese su nombre" requerid>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="telefono" id="telefono" placeholder="Ingrese su teléfono" requerid>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="correo" id="correo" placeholder="Ingrese su correo" requerid>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" mensaje="requerid" minlength="20" maxlength="200" name="mensaje" id="mensaje" cols="30" rows="10" placeholder="Escriba su mensaje..."></textarea>
                    </div>
                    
                    <div class="form-group">
                        <button type="submit" class="boton_02">Enviar</button>
                    </div>

                    <div id="alerta" class="alert d-none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!-- FOOTER -->
    @include('footers.footer_i')
    
@endsection