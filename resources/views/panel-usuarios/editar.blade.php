@extends('layouts.app_panel')

@section('content')
    
    @include('sidebars.sidebar_p_u')
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand">Panel de control</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">3</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-danger">
                  <h4 class="card-title">Editar Perfil</h4>
                  <p class="card-category">Completa el Perfil</p>
                </div>
                <div class="card-body">
                  <form method="post" action="/SG3_Usuarios/Editar_u">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Compañia (desabilitado)</label>
                          <input type="text" class="form-control" name="company" disabled value="SG3 Perú" required>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">DNI</label>
                          <input type="text" class="form-control" name="na_ident" value="{{$usuario->na_ident}}" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Correo</label>
                          <input type="email" class="form-control" name="email" value="{{$usuario->email}}" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombre</label>
                          <input type="text" class="form-control" name="prename" value="{{$usuario->prename}}" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Apellido</label>
                          <input type="text" class="form-control" name="name" value="{{$usuario->name}}" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nivel de Usuario</label>
                          <select class="form-control" name="category" required>
                            @if($usuario->category == 'Adm')
                              <option value="Adm">Administrador</option>
                              <option value="Edt">Editor</option>
                              <option value="Usu">Usuario</option>
                            @elseif($usuario->category == 'Edt')
                              <option value="Edt">Editor</option>
                              <option value="Adm">Administrador</option>
                              <option value="Usu">Usuario</option>
                            @else
                              <option value="Usu">Usuario</option>
                              <option value="Adm">Administrador</option>
                              <option value="Edt">Editor</option>
                            @endif                            
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Dirección</label>
                          <input type="text" class="form-control" name="adress" value="{{$usuario->adress}}" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Sobre mi</label>
                          <div class="form-group">
                            <textarea class="form-control" rows="2" name="about"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="id" value="{{$usuario->id}}">
                    <button type="submit" class="btn btn-danger pull-right">Actualizar Perfil</button>
                    <a href="/SG3_Usuarios"><button type="button" class="btn btn-danger pull-right">Atras</button></a>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo">
                    <img class="img" src="/assets/panel/img/faces/marc.jpg" />
                  </a>
                </div>
                <div class="card-body">
                  <h6 class="card-category text-gray">{{$usuario->category}}</h6>
                  <h4 class="card-title">{{$usuario->prename}} {{$usuario->name}}</h4>
                  <p class="card-description">
                    {{$usuario->about}}
                  </p>
                  @if($usuario->status == 'A')
                    <a href="/SG3_Usuarios/Desactivar_u/{{$usuario->id}}" class="btn btn-danger btn-round">Desactivar</a>
                  @else
                    <a href="/SG3_Usuarios/Activar_u/{{$usuario->id}}" class="btn btn-success btn-round">Activar</a>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <style>
    .content{
      margin-top: 65px !important;
    }
  </style>    
@endsection