@extends('layouts.app_panel')

@section('content')

    @include('sidebars.sidebar_p_u')
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand">Panel de control</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">3</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="container-fluid">
          <div class="row">            
            <div class="col-lg-6 col-md-12">
              <div class="card">
                <div class="card-header card-header-danger">
                  <div style="width: 80%; display: inline-block;">
                    <h4 class="card-title">Estadística de Usuarios</h4>
                    <p class="card-category">Cantidad de Colaboradores: {{$cont}}</p>
                  </div> 
                  @if(Auth::User()->category == 'Adm')
                    <div style="width: 18%; display: inline-block; vertical-align: top; text-align: right;">
                        <a href="/SG3_Usuarios/Agregar"><img class="button_m" src="/img/panel/mas.png"></a>
                    </div>                                   
                  @endif
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-danger" style="text-align: center;">
                      <th>Código</th>
                      <th>Estado</th>
                      <th>Nombres</th>
                      <th>Apellidos</th>
                      <th>DNI</th>
                      <th>Nivel de Usuario</th>
                      <th>Correo</th>
                      <th>Blog</th>
                      @if(Auth::User()->category == 'Adm')
                        <th>Acción</th>
                      @endif
                    </thead>
                    <tbody>
                      @foreach($usuarios as $usuario)
                        <tr style="text-align: center;">
                          <td>{{$usuario->id}}</td>
                          @if($usuario->status == 'A')
                            <td style="color: green"><b>Activo</b></td>
                          @else
                            <td style="color: red"><b>Inactivo</b></td>
                          @endif
                          <td>{{$usuario->prename}}</td>
                          <td>{{$usuario->name}}</td>
                          <td>{{$usuario->na_ident}}</td>   
                          @if($usuario->category == 'Adm')                       
                            <td>Administrador</td>
                          @elseif($usuario->category == 'Edt')  
                            <td>Editor</td>
                          @else
                            <td>Usuario</td>
                          @endif
                          <td>{{$usuario->email}}</td>                          
                          <td>580</td>
                          @if(Auth::user()->category == 'Adm')
                          <td>
                              <a href="/SG3_Usuarios/Activar_u/{{$usuario->id}}"><img src="/img/panel/verde.png" style="width: 24px; margin-right: 2%"></a>
                              <a href="/SG3_Usuarios/Desactivar_u/{{$usuario->id}}"><img src="/img/panel/rojo.png" style="width: 24px; margin-right: 2%"></a>
                              <a href="/SG3_Usuarios/Editar_u/{{$usuario->id}}"><img src="/img/panel/edit.png" style="width: 24px; margin-right: 2%"></a>
                          </td>
                          @endif
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <style>
        @media (min-width: 992px){
          .col-lg-6{
              flex: 0 0 99%;
              max-width: 100%;
          }
        }
        .button_m{
          width: 40px;
          transition: transform .2s;
        }
        .button_m:hover{
          transform: scale(1.1);
        }
      </style>
      <script src="/assets/panel/js/core/jquery.min.js" type="text/javascript"></script>
      <script src="/assets/panel/js/core/popper.min.js" type="text/javascript"></script>
      <script src="/assets/panel/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
      <script src="/assets/panel/js/plugins/perfect-scrollbar.jquery.min.js"></script>
      <!--  Google Maps Plugin    -->
      <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
      <!-- Chartist JS -->
      <script src="/assets/panel/js/plugins/chartist.min.js"></script>
      <!--  Notifications Plugin    -->
      <script src="/assets/panel/js/plugins/bootstrap-notify.js"></script>
      <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
      <script src="/assets/panel/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
      <!-- Material Dashboard DEMO methods, don't include it in your project! -->
      <script src="/assets/panel/demo/demo.js"></script>
      <script>
        $(document).ready(function() {
          // Javascript method's body can be found in assets/js/demos.js
          md.initDashboardPageCharts();

        });
      </script>
    
@endsection