@extends('layouts.app_panel')

@section('content')

    @include('sidebars.sidebar_p_c')
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand">Panel de control</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">3</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div class="content">
        <div class="container-fluid">          
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-danger">
                  <h4 class="card-title ">Configuración Página N°1</h4>
                  <p class="card-category"> Los cambios se muestran en la página principal</p>
                </div>
                <div class="card-body">
                  <nav class="navbar navbar-expand-lg navbar-light bg-light" style="box-shadow: none">           
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                          <a class="nav-link" href="/SG3_Config">General <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                          <a class="nav-link" href="/SG3_Config/Header">Header</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="/SG3_Config/Footer">Footer</a>
                        </li>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Web
                          </a>
                          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/SG3_Config/Pg1">Inicio</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg2">Nosotros</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg3">Servicios - Seguridad</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg4">Servicios - Asesorías</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg5">Blog</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg6">Contacto</a>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </nav>                  
                </div>
              </div>
              <div class="card" style="padding: 2%">
                <h4><b>Configuración de Slider</b></h4>
                <form action="/SG3_Config/Pg1" method="POST" enctype="multipart/form-data">
                  @csrf
                      <div class="left-g">
                          <div style="border: 1px solid gainsboro; padding: 2%; margin-bottom: 2%">
                            <div style="width: 50%; display: inline-block; vertical-align: top;">
                                <h4 style="width: 100%; text-align: center;"><b>Slider 1</b></h4>
                                <div class="control-div">
                                  <label class="label-nom">Titulo 1 </label>
                                  <div class="input-tit">
                                    <input type="text" class="form-control" style="text-align: center;" name="slide1_titulo1" value="{{$slide1->titulo1}}" required>
                                  </div> 
                                </div>   
                                <div class="control-div">
                                  <label class="label-nom">Titulo 2 </label>
                                  <div class="input-tit">
                                    <input type="text" class="form-control" style="text-align: center;" name="slide1_titulo2" value="{{$slide1->titulo2}}" required>
                                  </div> 
                                </div> 
                                <div class="control-div">
                                  <label class="label-nom">Texto </label>
                                  <div class="input-tit">
                                    <input type="text" class="form-control" style="text-align: center;" name="slide1_texto" value="{{$slide1->texto}}" required>
                                  </div> 
                                </div>
                                <div class="control-div">
                                    <div>
                                        <label class="label-nom">Imagen Slide </label>
                                        <div class="input-tit">
                                          <input type="file" class="form-control" style="text-align: center;" name="slide1">
                                        </div>                             
                                    </div>
                                </div>
                            </div>
                            <div style="width: 48%; display: inline-block; vertical-align: top; text-align: center;">
                              <img src="/{{$slide1->image}}" style="width: 55%">
                            </div>
                              
                          </div>     
                          <div style="border: 1px solid gainsboro; padding: 2%; margin-bottom: 2%">
                              <div style="width: 50%; display: inline-block; vertical-align: top;">
                                  <h4 style="width: 100%; text-align: center;"><b>Slider 2</b></h4>
                                  <div class="control-div">
                                    <label class="label-nom">Titulo 1 </label>
                                    <div class="input-tit">
                                      <input type="text" class="form-control" style="text-align: center;" name="slide2_titulo1" value="{{$slide2->titulo1}}" required>
                                    </div> 
                                  </div>   
                                  <div class="control-div">
                                    <label class="label-nom">Titulo 2 </label>
                                    <div class="input-tit">
                                      <input type="text" class="form-control" style="text-align: center;" name="slide2_titulo2" value="{{$slide2->titulo2}}" required>
                                    </div> 
                                  </div> 
                                  <div class="control-div">
                                    <label class="label-nom">Texto </label>
                                    <div class="input-tit">
                                      <input type="text" class="form-control" style="text-align: center;" name="slide2_texto" value="{{$slide2->texto}}" required>
                                    </div> 
                                  </div>
                                  <div class="control-div">
                                      <div>
                                          <label class="label-nom">Imagen Slide </label>
                                          <div class="input-tit">
                                            <input type="file" class="form-control" style="text-align: center;" name="slide2">
                                          </div>                             
                                      </div>
                                  </div>
                              </div>
                              <div style="width: 48%; display: inline-block; vertical-align: top; text-align: center;">
                                <img src="/{{$slide2->image}}" style="width: 55%">
                              </div>
                          </div>    
                          <div style="border: 1px solid gainsboro; padding: 2%; margin-bottom: 2%">
                              <div style="width: 50%; display: inline-block; vertical-align: top;">
                                  <h4 style="width: 100%; text-align: center;"><b>Slider 3</b></h4>
                                  <div class="control-div">
                                    <label class="label-nom">Titulo 1 </label>
                                    <div class="input-tit">
                                      <input type="text" class="form-control" style="text-align: center;" name="slide3_titulo1" value="{{$slide3->titulo1}}" required>
                                    </div> 
                                  </div>   
                                  <div class="control-div">
                                    <label class="label-nom">Titulo 2 </label>
                                    <div class="input-tit">
                                      <input type="text" class="form-control" style="text-align: center;" name="slide3_titulo2" value="{{$slide3->titulo2}}" required>
                                    </div> 
                                  </div> 
                                  <div class="control-div">
                                    <label class="label-nom">Texto </label>
                                    <div class="input-tit">
                                      <input type="text" class="form-control" style="text-align: center;" name="slide3_texto" value="{{$slide3->texto}}" required>
                                    </div> 
                                  </div>
                                  <div class="control-div">
                                      <div>
                                          <label class="label-nom">Imagen Slide </label>
                                          <div class="input-tit">
                                            <input type="file" class="form-control" style="text-align: center;" name="slide3">
                                          </div>                             
                                      </div>
                                  </div>
                              </div>
                              <div style="width: 48%; display: inline-block; vertical-align: top; text-align: center;">
                                <img src="/{{$slide3->image}}" style="width: 55%">
                              </div>
                          </div>     
                      </div>
                    <input type="submit" class="btn btn-danger pull-center" value="Guardar Cambios">
                  </form>
              </div>
              <div class="card" style="padding: 2%">
                <h4><b>Configuración Modulo 02</b></h4>
                <form action="/SG3_Config/Pg1/Mod02" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="control-div">
                      <label class="label-nom">Titulo </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="prd03_titulo" value="{{$prd03->titulo}}" required>
                      </div> 
                  </div>  
                  <div class="control-div">
                      <label class="label-nom">texto </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="prd03_texto" value="{{$prd03->texto}}" required>
                      </div> 
                  </div> 
                  <div>
                      <div style="width: 49%; display: inline-block; vertical-align: top; border: 1px solid gainsboro; padding: 2%; margin-top: 2%">
                          <h4 style="width: 100%; text-align: center;"><b>PRD 01</b></h4>
                          <div class="control-div">
                              <label class="label-nom">Titulo 1 </label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd01_titulo" value="{{$prd01->titulo}}" required>
                                </div> 
                          </div>   
                          <div class="control-div">
                              <label class="label-nom">Texto</label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd01_texto" value="{{$prd01->texto}}" required>
                                </div> 
                          </div>  
                          <div class="control-div">
                              <label class="label-nom">Link</label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd01_link" value="{{$prd01->link}}" required>
                                </div> 
                          </div>                                 
                          <div class="control-div">
                                <div>
                                  <label class="label-nom">Imagen</label>
                                  <div class="input-tit">
                                      <input type="file" class="form-control" style="text-align: center;" name="prd01">
                                  </div>                             
                                </div>
                          </div>
                          <div style="width: 100%; text-align: center; padding: 2%">
                            <img src="/{{$prd01->image}}" style="width: 55%">
                          </div>                         
                      </div>
                      <div style="width: 49%; display: inline-block; vertical-align: top; border: 1px solid gainsboro; padding: 2%; margin-top: 2%">
                          <h4 style="width: 100%; text-align: center;"><b>PRD 02</b></h4>
                          <div class="control-div">
                              <label class="label-nom">Titulo 1 </label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd02_titulo" value="{{$prd02->titulo}}" required>
                                </div> 
                          </div>   
                          <div class="control-div">
                              <label class="label-nom">Texto</label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd02_texto" value="{{$prd02->texto}}" required>
                                </div> 
                          </div>  
                          <div class="control-div">
                              <label class="label-nom">Link</label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd02_link" value="{{$prd02->link}}" required>
                                </div> 
                          </div>                                  
                          <div class="control-div">
                                <div>
                                  <label class="label-nom">Imagen</label>
                                  <div class="input-tit">
                                      <input type="file" class="form-control" style="text-align: center;" name="prd02">
                                  </div>                             
                                </div>
                          </div>
                          <div style="width: 100%; text-align: center; padding: 2%">
                            <img src="/{{$prd02->image}}" style="width: 55%">
                          </div>                         
                      </div>
                  </div> 
                <input type="submit" class="btn btn-danger pull-center" value="Guardar Cambios"> 
                </form>
              </div>
              <div class="card" style="padding: 2%">
                <h4><b>Configuración Modulo 03</b></h4>
                <form action="/SG3_Config/Pg1/Mod03" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="control-div" style="margin-top: 2%">
                      <label class="label-nom">Titulo </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="mod03_titulo" value="{{$mod03->titulo}}" required>
                      </div> 
                  </div>                    
                  <div>
                    <div class="table-responsive">
                      <table class="table" id="tabla">
                        <thead class=" text-danger">
                          <th>ID</th>
                          <th>Nombre</th>
                          <th>Imagen</th>
                          <th>Opción</th>
                        </thead>
                        <tbody>
                          @foreach($certificados as $certificado)
                          <tr>
                            <td>{{$certificado->id}}</td>
                            <td>{{$certificado->titulo}}</td>
                            <td><img src="/{{$certificado->image}}" style="width: 32px"></td>
                            <td>
                              <a href="/SG3_Config/Pg1/CertificadoEl/{{$certificado->id}}"><img src="/img/panel/tacho.svg" style="width: 24px; margin-right: 2%"></a>
                            </td>
                          </tr> 
                          @endforeach                       
                        </tbody>
                      </table>
                    </div>
                  </div> 
                  <button type="button" class="btn btn-danger pull-center" onclick="agregarRow()">Nuevo Certificado</button> 
                  <input type="submit" class="btn btn-danger pull-center" value="Guardar Cambios"> 
                </form>
              </div>
              <div class="card" style="padding: 2%">
                <h4><b>Configuración Modulo 04</b></h4>
                <form action="/SG3_Config/Pg1/Mod04" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="control-div">
                      <label class="label-nom">Titulo </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="mod04_titulo" value="{{$mod04->titulo}}" required>
                      </div> 
                  </div>  
                  <div class="control-div">
                      <label class="label-nom">texto </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="mod04_texto" value="{{$mod04->texto}}" required>
                      </div> 
                  </div> 
                  <div class="control-div">
                      <label class="label-nom">Link </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="mod04_link" value="{{$mod04->link}}" required>
                      </div> 
                  </div> 
                  <input type="submit" class="btn btn-danger pull-center" value="Guardar Cambios"> 
                </form>
              </div>
              <div class="card" style="padding: 2%">
                <h4><b>Configuración Modulo 05</b></h4>
                <form action="/SG3_Config/Pg1/Mod05" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="control-div" style="margin-top: 2%">
                      <label class="label-nom">Titulo </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="mod05_titulo" value="{{$mod05->titulo}}" required>
                      </div> 
                  </div>                    
                  <div>
                    <div class="table-responsive">
                      <table class="table" id="tabla_espe">
                        <thead class=" text-danger">
                          <th>ID</th>
                          <th>Nombre</th>
                          <th>Imagen</th>
                          <th>Opción</th>
                        </thead>
                        <tbody>
                          @foreach($especialidades as $especialidad)
                          <tr>
                            <td>{{$especialidad->id}}</td>
                            <td>{{$especialidad->titulo}}</td>
                            <td><img src="/{{$especialidad->image}}" style="width: 32px"></td>
                            <td>
                              <a href="/SG3_Config/Pg1/EspecialidadEl/{{$especialidad->id}}"><img src="/img/panel/tacho.svg" style="width: 24px; margin-right: 2%"></a>
                            </td>
                          </tr> 
                          @endforeach                       
                        </tbody>
                      </table>
                    </div>
                  </div> 
                  <button type="button" class="btn btn-danger pull-center" onclick="agregarRow_espe()">Nueva Especialidad</button> 
                  <input type="submit" class="btn btn-danger pull-center" value="Guardar Cambios"> 
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <script>
        function statusOn(){
          document.getElementById('off').className = 'btn btn-secondary button-status';
          document.getElementById('on').className = 'btn btn-success button-status';
          document.getElementById('status_page').value = 'ON';
        }
        function statusOff(){
          document.getElementById('off').className = 'btn btn-danger button-status';
          document.getElementById('on').className = 'btn btn-secondary button-status';
          document.getElementById('status_page').value = 'OFF';
        }
        function agregarRow(){
          var tds=$("#tabla tr:first td").length;
          var trs=$("#tabla tr").length;
            var nuevaFila="<tr>";
            nuevaFila+= "<td>+</td>"+
                        "<td><input type='text' class='form-control' style='text-align: center; width: 80%' name='mod_03_titulo_n' required></td>"+
                        "<td><input type='file' class='form-control' style='text-align: center; width: 80%' name='mod_03_img_n'></td>";
            nuevaFila+="</tr>";
            $("#tabla").append(nuevaFila);
        }
        function agregarRow_espe(){
          var tds=$("#tabla_espe tr:first td").length;
          var trs=$("#tabla_espe tr").length;
            var nuevaFila="<tr>";
            nuevaFila+= "<td>+</td>"+
                        "<td><input type='text' class='form-control' style='text-align: center; width: 80%' name='mod_05_titulo_n' required></td>"+
                        "<td><input type='file' class='form-control' style='text-align: center; width: 80%' name='mod_05_img_n'></td>";
            nuevaFila+="</tr>";
            $("#tabla_espe").append(nuevaFila);
        }
      </script>
      <style>
        .left-g{
          width: 100%;
          display: inline-block;
          vertical-align: top;
        }
        .right-g{
          width: 53%;
          display: inline-block;
          vertical-align: top;
        }
        .label-nom{
          display: inline-block; 
          width: 30%
        }
        .input-tit{
          width: 65%; 
          display: inline-block;
        }
        .status{
          width: 65%;
          border: 1px solid gainsboro;
        }
        .button-status{
          width: 50%;
          padding-top: 8px;
          padding-bottom: 8px;
        }
        .control-div{
          height: 40px;
        }.control-div-r{
          height: 40px;
        }
        @media (min-width: 992px){
          .col-lg-6{
              flex: 0 0 99%;
              max-width: 100%;
          }
        }
        .button_m{
          width: 40px;
          transition: transform .2s;
        }
        .button_m:hover{
          transform: scale(1.1);
        }
      </style>
      <script src="/assets/panel/js/core/jquery.min.js" type="text/javascript"></script>
      <script src="/assets/panel/js/core/popper.min.js" type="text/javascript"></script>
      <script src="/assets/panel/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
      <script src="/assets/panel/js/plugins/perfect-scrollbar.jquery.min.js"></script>
      <!--  Google Maps Plugin    -->
      <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
      <!-- Chartist JS -->
      <script src="/assets/panel/js/plugins/chartist.min.js"></script>
      <!--  Notifications Plugin    -->
      <script src="/assets/panel/js/plugins/bootstrap-notify.js"></script>
      <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
      <script src="/assets/panel/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
      <!-- Material Dashboard DEMO methods, don't include it in your project! -->
      <script src="/assets/panel/demo/demo.js"></script>
      <script>
        $(document).ready(function() {
          // Javascript method's body can be found in assets/js/demos.js
          md.initDashboardPageCharts();

        });
      </script>
    
@endsection