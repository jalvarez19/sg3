@extends('layouts.app_panel')

@section('content')

    @include('sidebars.sidebar_p_c')
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand">Panel de control</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">3</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div class="content">
        <div class="container-fluid">          
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-danger">
                  <h4 class="card-title ">Configuración Página N°2</h4>
                  <p class="card-category"> Los cambios se muestran en la página N° 2</p>
                </div>
                <div class="card-body">
                  <nav class="navbar navbar-expand-lg navbar-light bg-light" style="box-shadow: none">           
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                          <a class="nav-link" href="/SG3_Config">General <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                          <a class="nav-link" href="/SG3_Config/Header">Header</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="/SG3_Config/Footer">Footer</a>
                        </li>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Web
                          </a>
                          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/SG3_Config/Pg1">Inicio</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg2">Nosotros</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg3">Servicios - Seguridad</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg4">Servicios - Asesorías</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg5">Blog</a>
                            <a class="dropdown-item" href="/SG3_Config/Pg6">Contacto</a>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </nav>                  
                </div>
              </div>
              <div class="card" style="padding: 2%">
                <h4><b>Configuración Modulo 01</b></h4>
                <form action="/SG3_Config/Pg2/Mod01" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="control-div">
                      <label class="label-nom">Titulo </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="prd03_titulo" value="{{$prd03->titulo}}" required>
                      </div> 
                  </div>  
                  <div class="control-div">
                      <label class="label-nom">Head </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="prd03_head" value="{{$prd03->head}}" required>
                      </div> 
                  </div> 
                  <div class="control-div">
                      <label class="label-nom">texto </label>
                      <div class="input-tit">
                          <input type="text" class="form-control" style="text-align: center;" name="prd03_texto" value="{{$prd03->texto}}" required>
                      </div> 
                  </div> 
                  <div>
                      <div style="width: 49%; display: inline-block; vertical-align: top; border: 1px solid gainsboro; padding: 2%; margin-top: 2%">
                          <h4 style="width: 100%; text-align: center;"><b>PRD 01</b></h4>
                          <div class="control-div">
                              <label class="label-nom">Titulo 1 </label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd01_titulo" value="{{$prd01->titulo}}" required>
                                </div> 
                          </div>   
                          <div class="control-div">
                              <label class="label-nom">Texto</label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd01_texto" value="{{$prd01->texto}}" required>
                                </div> 
                          </div>                                  
                          <div class="control-div">
                                <div>
                                  <label class="label-nom">Imagen</label>
                                  <div class="input-tit">
                                      <input type="file" class="form-control" style="text-align: center;" name="prd01">
                                  </div>                             
                                </div>
                          </div>
                          <div style="width: 100%; text-align: center; padding: 2%">
                            <img src="/{{$prd01->image}}" style="width: 30%">
                          </div>                         
                      </div>
                      <div style="width: 49%; display: inline-block; vertical-align: top; border: 1px solid gainsboro; padding: 2%; margin-top: 2%">
                          <h4 style="width: 100%; text-align: center;"><b>PRD 02</b></h4>
                          <div class="control-div">
                              <label class="label-nom">Titulo 1 </label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd02_titulo" value="{{$prd02->titulo}}" required>
                                </div> 
                          </div>   
                          <div class="control-div">
                              <label class="label-nom">Texto</label>
                                <div class="input-tit">
                                  <input type="text" class="form-control" style="text-align: center;" name="prd02_texto" value="{{$prd02->texto}}" required>
                                </div> 
                          </div>                                    
                          <div class="control-div">
                                <div>
                                  <label class="label-nom">Imagen</label>
                                  <div class="input-tit">
                                      <input type="file" class="form-control" style="text-align: center;" name="prd02">
                                  </div>                             
                                </div>
                          </div>
                          <div style="width: 100%; text-align: center; padding: 2%">
                            <img src="/{{$prd02->image}}" style="width: 30%">
                          </div>                         
                      </div>
                  </div> 
                <input type="submit" class="btn btn-danger pull-center" value="Guardar Cambios"> 
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <style>
        .left-g{
          width: 100%;
          display: inline-block;
          vertical-align: top;
        }
        .right-g{
          width: 53%;
          display: inline-block;
          vertical-align: top;
        }
        .label-nom{
          display: inline-block; 
          width: 30%
        }
        .input-tit{
          width: 65%; 
          display: inline-block;
        }
        .status{
          width: 65%;
          border: 1px solid gainsboro;
        }
        .button-status{
          width: 50%;
          padding-top: 8px;
          padding-bottom: 8px;
        }
        .control-div{
          height: 40px;
        }.control-div-r{
          height: 40px;
        }
        @media (min-width: 992px){
          .col-lg-6{
              flex: 0 0 99%;
              max-width: 100%;
          }
        }
        .button_m{
          width: 40px;
          transition: transform .2s;
        }
        .button_m:hover{
          transform: scale(1.1);
        }
      </style>
      <script src="/assets/panel/js/core/jquery.min.js" type="text/javascript"></script>
      <script src="/assets/panel/js/core/popper.min.js" type="text/javascript"></script>
      <script src="/assets/panel/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
      <script src="/assets/panel/js/plugins/perfect-scrollbar.jquery.min.js"></script>
      <!--  Google Maps Plugin    -->
      <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
      <!-- Chartist JS -->
      <script src="/assets/panel/js/plugins/chartist.min.js"></script>
      <!--  Notifications Plugin    -->
      <script src="/assets/panel/js/plugins/bootstrap-notify.js"></script>
      <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
      <script src="/assets/panel/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
      <!-- Material Dashboard DEMO methods, don't include it in your project! -->
      <script src="/assets/panel/demo/demo.js"></script>
      <script>
        $(document).ready(function() {
          // Javascript method's body can be found in assets/js/demos.js
          md.initDashboardPageCharts();

        });
      </script>
    
@endsection