<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="SG3 Perú">
    <meta name="author" content="CREATOS">
    <link rel="shortcut icon" href="/img/favicon.png">

    <title>SG3 Perú - Login</title>

    <!-- Bootstrap CSS -->    
    <link href="/css/login/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="/css/login/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="/css/login/elegant-icons-style.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="/css/login/style.css" rel="stylesheet">
    <link href="/css/login/style-responsive.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-img3-body">

    <div class="container">
      <form class="login-form" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf       
        <div class="login-wrap">
            <div style="width: 100%; text-align: center;">
                <img src="img/logo-02.svg" alt="Logo Sg3" style="width: 50%; margin-bottom: 3%">
            </div>            
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="text" placeholder="Correo" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autofocus>                
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required  placeholder="Password">                
            </div>
            <label class="checkbox">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </label>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
            <div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
      </form>

    </div>


  </body>
</html>
