<header id="menu" class="contenedor_header fixed-top">
        <nav class="navbar navbar-expand-lg">
            <a id="logo" class="navbar-brand" href="index.html">
                <img src="/{{$config->logo_top}}" alt="Logo Sg3">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_header" aria-controls="menu_header"
                aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger hamburger--spring">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="menu_header">
                <div class="navbar-nav">
                    <a class="nav-item nav-link animated fadeIn" href="/Inicio"><i class="fas fa-home"></i> {{$header->pg1}}</a>
                    <span class="d-none d-xl-block animated fadeIn">/</span>
                    <a class="nav-item nav-link animated fadeIn" href="/Nosotros"><i class="fas fa-users"></i> {{$header->pg2}}</a>
                    <span class="d-none d-xl-block animated fadeIn">/</span>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="menu_despegable_02" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-shield-alt"></i> {{$header->pg3}}
                        </a>
                        <div class="dropdown-menu animated fadeInLeft" aria-labelledby="menu_despegable_02">
                            <a class="dropdown-item" href="/Seguridad">{{$header->subpg3_1}}</a>
                            <a class="dropdown-item " href="/Asesorias">{{$header->subpg3_2}}</a>
                        </div>
                    </li>
                    <span class="d-none d-xl-block animated fadeIn">/</span>
                    <a class="nav-item nav-link animated fadeIn  menu_activado" href="/Blog"><i class="fas fa-book-open"></i> {{$header->pg4}}</a>
                    <span class="d-none d-xl-block animated fadeIn">/</span>
                    <a class="nav-item nav-link animated fadeIn" href="/Contacto"><i class="fas fa-phone-volume"></i> {{$header->pg5}}</a>
                </div>
            </div>
        </nav>
    </header>