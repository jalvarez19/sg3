// MENU ANIMACION JS

var forEach = function (t, o, r) {
    if ("[object Object]" === Object.prototype.toString.call(t))
        for (var c in t) Object.prototype.hasOwnProperty.call(t, c) && o.call(r, t[c], c, t);
    else
        for (var e = 0, l = t.length; l > e; e++) o.call(r, t[e], e, t)
};

var hamburgers = document.querySelectorAll(".hamburger");
if (hamburgers.length > 0) {
    forEach(hamburgers, function (hamburger) {
        hamburger.addEventListener("click", function () {
            this.classList.toggle("is-active");
        }, false);
    });
}

//////////////////////////////////////////////////////////////////////////////////////////////////

// CAMBIO ESTILOS SCROLL
$(window).scroll(function () {
    if ($("#logo").offset().top > 20) {
        $("#logo").addClass("logo_width");
    } else {
        $("#logo").removeClass("logo_width");
    }
});


$(window).scroll(function () {
    if ($("#menu").offset().top > 20) {
        $("#menu").addClass("fondo_header_mostrar");
    } else {
        $("#menu").removeClass("fondo_header_mostrar");
    }
});

//////////////////////////////////////////////////////////////////////////////////////////////////

// LIGHT BOX

$(document).on('click', '[data-toggle="lightbox"]', function (event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

//////////////////////////////////////////////////////////////////////////////////////////////////

// ANIMACIONES

new WOW().init();

//////////////////////////////////////////////////////////////////////////////////////////////////

// TIEMPO SLIDE

$('.carousel').carousel({
    interval: 4000
})

