-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-08-2018 a las 00:23:57
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sg3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `editor` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` varchar(9000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vistas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `blog`
--

INSERT INTO `blog` (`id`, `estado`, `editor`, `titulo`, `texto`, `tipo`, `vistas`, `image`, `created_at`, `updated_at`) VALUES
(1, '1', 'Jairo A', 'Noticias 01', '<p class=\"texto_blog\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae impedit unde voluptates obcaecati tempora.</p>\r\n                    <p class=\"texto_blog\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae impedit unde voluptates obcaecati tempora necessitatibus itaque nisi perspiciatis praesentium. Cupiditate cum distinctio.</p>\r\n                    <p class=\"texto_blog\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae impedit unde voluptates obcaecati tempora necessitatibus itaque nisi perspiciatis praesentium. Cupiditate cum distinctio.</p>       \r\n                    <p class=\"texto_blog\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae impedit unde voluptates obcaecati tempora.</p> ', '1', '33', 'img/panel/slide1.png', NULL, NULL),
(2, '1', 'Jairo A', 'Noticias 02', '<p class=\"texto_blog\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae impedit unde voluptates obcaecati tempora.</p>\r\n                    <p class=\"texto_blog\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae impedit unde voluptates obcaecati tempora necessitatibus itaque nisi perspiciatis praesentium. Cupiditate cum distinctio.</p>\r\n                    <p class=\"texto_blog\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae impedit unde voluptates obcaecati tempora necessitatibus itaque nisi perspiciatis praesentium. Cupiditate cum distinctio.</p>       \r\n                    <p class=\"texto_blog\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae impedit unde voluptates obcaecati tempora.</p> ', '1', '55', 'img/panel/slide2.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config`
--

CREATE TABLE `config` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descript` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_top` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_fot` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config`
--

INSERT INTO `config` (`id`, `site_name`, `status`, `descript`, `keywords`, `logo_top`, `logo_fot`, `created_at`, `updated_at`) VALUES
(2, 'SG3 Perú', 'ON', 'SG3 Perú descripción', 'SG3, SEGURIDAD, PERU, LIMA, BLOG', 'img/panel/logo.png', 'img/panel/logo_footer.png', '2018-08-06 21:13:16', '2018-08-07 20:19:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_footer`
--

CREATE TABLE `config_footer` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telef_f` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telef_c` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubica` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_footer`
--

INSERT INTO `config_footer` (`id`, `titulo`, `telef_f`, `telef_c`, `ubica`, `fb`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'CONTACTO', '+511 01 713 1691', '+51 961 430 698', 'Lima - Perú', 'Sg3 Perú', 'img/panel/logo_footer.png', NULL, '2018-08-07 10:02:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_header`
--

CREATE TABLE `config_header` (
  `id` int(10) UNSIGNED NOT NULL,
  `pg1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pg2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pg3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subpg3_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subpg3_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pg4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pg5` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_header`
--

INSERT INTO `config_header` (`id`, `pg1`, `pg2`, `pg3`, `subpg3_1`, `subpg3_2`, `pg4`, `pg5`, `created_at`, `updated_at`) VALUES
(1, 'Inicio', 'Nosotros', 'Servicios', 'Seguridad', 'Asesorías', 'Blog', 'Contacto', NULL, '2018-08-06 22:44:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_mod01_nosotros`
--

CREATE TABLE `config_mod01_nosotros` (
  `id` int(10) UNSIGNED NOT NULL,
  `head` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_mod01_nosotros`
--

INSERT INTO `config_mod01_nosotros` (`id`, `head`, `titulo`, `texto`, `image`, `link`, `created_at`, `updated_at`) VALUES
(1, 'SG3 Perú,', 'ESPECIALISTAS EN SEGURIDAD', 'empresa especializada en brindar Servicios de Seguridad y Vigilancia Privada según las diferentes necesidades de nuestros clientes. Somos la única empresa con el objetivo principal de brindar soluciones factibles y eficaces, enfocando nuestro servicio de forma personalizada y generando satisfacción de confianza a nuestros Clientes.SG3 Perú, empresa especializada en brindar Servicios de Seguridad y Vigilancia Privada según las diferentes necesidades de nuestros clientes. Somos la única empresa con el objetivo principal de brindar soluciones factibles y eficaces, enfocando nuestro servicio de forma personalizada y generando satisfacción de confianza a nuestros Clientes.', '', '', NULL, NULL),
(2, '', 'MISIÓN', 'Brindar a nuestros clientes soluciones rápidas a sus necesidades, con personal eficiente y motivado, permitiendo su tranquilidad, satisfacción, confianza y calidad en el servicio.', 'img/panel/nosotros_prd01.png', '', NULL, '2018-08-07 18:43:56'),
(3, '', 'VISIÓN', 'Ser identificado como la empresa más confiable en servicios de seguridad privada y asesorar en base a principios de protección, ofreciendo soluciones integrales a nuestros clientes.', 'img/panel/nosotros_prd02.png', '', NULL, '2018-08-07 18:44:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_mod01_pg3`
--

CREATE TABLE `config_mod01_pg3` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` varchar(3000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(3000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_mod01_pg3`
--

INSERT INTO `config_mod01_pg3` (`id`, `titulo`, `texto`, `image`, `created_at`, `updated_at`) VALUES
(1, 'SEGURIDAD Y VIGILANCIA PRIVADA', 'Ofrecemos seguridad privada basada en el enfoque de la prevención de riesgos patrimoniales, basados en una amplia experiencia.Reconocidos por brindar soluciones en seguridad basados en el manejo de la gestión de riesgos, administración de la gestión humana, tecnología de la información y nuestra cadena de suministros.', 'Nuestro personal destaca por su capacitación para brindar soluciones en prevención de riesgos patrimoniales, en ese sentido se especializan y creamos estrategias para los diferentes servicios:', NULL, '2018-08-07 20:06:29'),
(2, 'SEGURIDAD A INSTALACIONES PÚBLICAS Y PRIVADAS', '', 'img/panel/instalaciones.png', '2018-08-07 19:58:26', '2018-08-07 19:58:26'),
(3, 'SEGURIDAD A CENTROS FINANCIEROS', '', 'img/panel/financieros.png', '2018-08-07 20:03:35', '2018-08-07 20:03:35'),
(4, 'SEGURIDAD A CENTROS EDUCATIVOS', '', 'img/panel/educativos.png', '2018-08-07 20:03:44', '2018-08-07 20:03:44'),
(5, 'RESGUARDO Y PROTECCIÓN', '', 'img/panel/resguardo.png', '2018-08-07 20:03:53', '2018-08-07 20:03:53'),
(6, 'SEGURIDAD A EVENTOS CORPORATIVOS', '', 'img/panel/eventos.png', '2018-08-07 20:08:30', '2018-08-07 20:08:30'),
(7, 'SEGURIDAD A EVENTOS PÚBLICOS', '', 'img/panel/seguridad-auto.png', '2018-08-07 20:08:44', '2018-08-07 20:08:44'),
(8, 'SEGURIDAD MÓVIL DE RESGUARDO PARA CONTENEDORES', '', 'img/panel/instalaciones.png', '2018-08-07 20:08:56', '2018-08-07 20:08:56'),
(9, 'SUPERVISIÓN ELECTRÓNICA DRONE', '', 'img/panel/drone.png', '2018-08-07 20:09:10', '2018-08-07 20:09:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_mod01_pg4`
--

CREATE TABLE `config_mod01_pg4` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_mod01_pg4`
--

INSERT INTO `config_mod01_pg4` (`id`, `texto`, `image`, `created_at`, `updated_at`) VALUES
(1, 'ENTRE LAS PRINCIPALES PODEMOS DESTACAR', '', NULL, NULL),
(2, 'Asesoría en disciplinas complementarias de Seguridad.', 'img/panel/asesoria01.png', '2018-08-08 18:25:31', '2018-08-08 18:25:31'),
(3, 'Asesoría de prevención y supervisión en el campo de trabajo para todo tipo de actividades.', 'img/panel/asesoria02.png', '2018-08-08 18:25:50', '2018-08-08 18:25:50'),
(4, 'Perfiles y protocolos de seguridad.', 'img/panel/asesoria03.png', '2018-08-08 18:26:01', '2018-08-08 18:26:01'),
(5, 'Acciones correctivas y mantenimiento del Sistema de Gestión en Seguridad BASC V004 / ISO 28000 Gestión de la Seguridad.', 'img/panel/asesoria04.png', '2018-08-08 18:26:10', '2018-08-08 18:26:10'),
(6, 'Diagnostico o estudio de línea base en seguridad patrimonial.', 'img/panel/asesoria05.png', '2018-08-08 18:26:20', '2018-08-08 18:26:20'),
(7, 'Formación de brigadas de emergencias.', 'img/panel/asesoria06.png', '2018-08-08 18:26:29', '2018-08-08 18:26:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_mod02`
--

CREATE TABLE `config_mod02` (
  `id` int(10) UNSIGNED NOT NULL,
  `prd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_mod02`
--

INSERT INTO `config_mod02` (`id`, `prd`, `titulo`, `texto`, `image`, `link`, `created_at`, `updated_at`) VALUES
(1, '1', 'SEGURIDAD  Y  VIGILANCIA', 'Ofrecemos servicio de vigilancia las 24 horas del dia para que tu empresa este con total cuidado.', 'img/panel/prd01.png', '/Seguridad', NULL, '2018-08-07 01:36:52'),
(2, '2', 'ASESORÍA Y CAPACITACIÓN', 'Capacitamos a tu personal, para que brinde un mejor servicio funcional en tu empresa.', 'img/panel/prd02.png', '/Asesorias', NULL, '2018-08-07 01:37:06'),
(3, '3', 'ESPECIALISTAS EN SEGURIDAD', 'Contamos con servicios de vigilancia y de asesorías, con los cuales puedes dejar la SEGURIDAD de tu empresa en nuestras manos.', '', '', NULL, '2018-08-07 01:38:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_mod03`
--

CREATE TABLE `config_mod03` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_mod03`
--

INSERT INTO `config_mod03` (`id`, `titulo`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Nuestras certificaciones', '', NULL, NULL),
(5, 'MEGA', 'img/panel/cer01.png', '2018-08-07 08:04:24', '2018-08-07 08:04:24'),
(6, 'BASC', 'img/panel/cer02.png', '2018-08-07 08:04:47', '2018-08-07 08:04:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_mod04`
--

CREATE TABLE `config_mod04` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_mod04`
--

INSERT INTO `config_mod04` (`id`, `titulo`, `texto`, `link`, `created_at`, `updated_at`) VALUES
(1, 'VISITA NUESTRO BLOG', 'En nuestro blog encontraras todo tipo de consejos y tips de seguridad que te ayudarán a tener una mejor perspectiva de como mantenerte seguro en tu vida persona y empresarial.... TE ESPERAMOS!', '/Blog', NULL, '2018-08-07 07:55:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_mod05`
--

CREATE TABLE `config_mod05` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_mod05`
--

INSERT INTO `config_mod05` (`id`, `titulo`, `image`, `created_at`, `updated_at`) VALUES
(1, 'NUESTRAS ESPECIALIDADES', '', NULL, '2018-08-07 08:34:02'),
(2, 'CONFIANZA', 'img/panel/confianza.png', '2018-08-07 08:28:38', '2018-08-07 08:28:38'),
(3, 'INNOVACIÓN', 'img/panel/innovacion.png', '2018-08-07 08:31:06', '2018-08-07 08:31:06'),
(4, 'LIDERAZGO', 'img/panel/liderazgo.png', '2018-08-07 08:31:28', '2018-08-07 08:31:28'),
(6, 'RECONOCIMIENTO', 'img/panel/reconocimiento.png', '2018-08-07 08:35:26', '2018-08-07 08:35:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_slide`
--

CREATE TABLE `config_slide` (
  `id` int(10) UNSIGNED NOT NULL,
  `slide` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo1` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo2` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config_slide`
--

INSERT INTO `config_slide` (`id`, `slide`, `titulo1`, `titulo2`, `texto`, `image`, `created_at`, `updated_at`) VALUES
(1, '1', 'SEGURIDAD', 'PARA EMPRESAS', 'SG3 Perú, empresa especializada en brindar Servicios de Seguridad y Vigilancia Privada según las diferentes necesidades de nuestros clientes.', 'img/panel/slide1.png', NULL, '2018-08-07 00:32:52'),
(2, '2', 'MONITOREO', 'CCTV / GPS', 'SG3 Peru cuenta con sistemas digitales de vídeo y monitoreo GPS que forman parte de la estructura en la prevención de riesgos patrimoniales y control de pérdidas.', 'img/panel/slide2.png', NULL, '2018-08-07 00:22:44'),
(3, '3', 'Seguridad', 'Y VIGILANCIA PRIVADA', 'Ofrecemos seguridad privada basada en el enfoque de la prevención de riesgos patrimoniales, basados en una amplia experiencia.', 'img/panel/slide3.png', NULL, '2018-08-07 00:22:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contac`
--

CREATE TABLE `contac` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mensaje` varchar(3000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adjunto` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contac`
--

INSERT INTO `contac` (`id`, `tipo`, `nombr`, `telef`, `email`, `mensaje`, `adjunto`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Trb', 'Jairo', '936858420', 'asdasddas@asdsd.com', 'BUSCO TRABAJO', '', '1', '2018-08-07 09:19:43', '2018-08-07 09:19:43'),
(2, 'Emp', 'Jairo', '936858420', 'jairoalvarezruiz@gmail.com', 'Quiero mi empresa segura', '', '1', '2018-08-07 19:20:43', '2018-08-07 19:20:43'),
(3, 'Emp', 'Jairo', '936858420', 'jairoalvarezruiz@gmail.com', 'sdasdasdasdffdsfdsfdsfdsfdsfds', '', '1', '2018-08-07 20:10:14', '2018-08-07 20:10:14'),
(4, 'Emp', 'Jairo', '972533322', 'jairoalvarezruiz@gmail.com', 'sdsdadsadsaaaaaasdsasds', '', '1', '2018-08-07 20:11:22', '2018-08-07 20:11:22'),
(5, 'Emp', 'Jairo', '3232332', 'jairoalvarezruiz@gmail.com', 'saddsaddsadsadsadsadsadsadsadsadsadsadsadsadsa', '', '1', '2018-08-07 20:13:24', '2018-08-07 20:13:24'),
(6, 'Emp', 'Jairo', '3232332', 'jairoalvarezruiz@gmail.com', 'saddsaddsadsadsadsadsadsadsadsadsadsadsadsadsa', '', '1', '2018-08-07 20:13:47', '2018-08-07 20:13:47'),
(10, 'Emp', 'Jairo', '936858420', 'pruebaasesorias@gmail.com', 'asdasdsaddaasdasdsaddaasdasdsaddaasdasdsaddaasdasdsaddaasdasdsadda', '', '1', '2018-08-08 18:31:15', '2018-08-08 18:31:15'),
(11, 'Emp', 'Jairo', '936858420', 'pruebaasesorias2@gmail.com', 'asddsdsdsadsasddsdsdsadsasddsdsdsadsasddsdsdsads', '', '1', '2018-08-08 18:32:18', '2018-08-08 18:32:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_06_050847_create_config_table', 1),
(4, '2018_08_06_172348_create_config_header_table', 2),
(5, '2018_08_06_185654_create_config_slide_table', 3),
(6, '2018_08_06_200058_create_config_mod02_table', 4),
(7, '2018_08_07_021146_create_config_mod03_table', 5),
(8, '2018_08_07_024809_create_config_mod04_table', 6),
(9, '2018_08_07_032040_create_config_mod05_table', 7),
(10, '2018_08_07_041221_create_contac_table', 8),
(11, '2018_08_07_044843_create_config_footer_table', 9),
(12, '2018_08_07_133229_create_config_mod01_nosotros_table', 10),
(13, '2018_08_07_145155_create_config_mod01_pg3_table', 11),
(14, '2018_08_08_131724_create_config_mod01_pg4_table', 12),
(15, '2018_08_08_185013_create_blog_table', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `prename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `na_ident` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `prename`, `name`, `category`, `status`, `na_ident`, `email`, `about`, `adress`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jairo', 'Alvarez', 'Adm', 'A', '48117811', 'jairoalvarezruiz@gmail.com', 'PROGRAMADOR', 'Lima 11', '$2y$10$L/KJLsUTI9eupq0lCezChezPLU7oT13Q6LfEBpUx8Tk0yb5aWeUeO', NULL, '2018-08-06 18:27:54', '2018-08-06 21:06:11');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_footer`
--
ALTER TABLE `config_footer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_header`
--
ALTER TABLE `config_header`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_mod01_nosotros`
--
ALTER TABLE `config_mod01_nosotros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_mod01_pg3`
--
ALTER TABLE `config_mod01_pg3`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_mod01_pg4`
--
ALTER TABLE `config_mod01_pg4`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_mod02`
--
ALTER TABLE `config_mod02`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_mod03`
--
ALTER TABLE `config_mod03`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_mod04`
--
ALTER TABLE `config_mod04`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_mod05`
--
ALTER TABLE `config_mod05`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_slide`
--
ALTER TABLE `config_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contac`
--
ALTER TABLE `contac`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `config_footer`
--
ALTER TABLE `config_footer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `config_header`
--
ALTER TABLE `config_header`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `config_mod01_nosotros`
--
ALTER TABLE `config_mod01_nosotros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `config_mod01_pg3`
--
ALTER TABLE `config_mod01_pg3`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `config_mod01_pg4`
--
ALTER TABLE `config_mod01_pg4`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `config_mod02`
--
ALTER TABLE `config_mod02`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `config_mod03`
--
ALTER TABLE `config_mod03`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `config_mod04`
--
ALTER TABLE `config_mod04`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `config_mod05`
--
ALTER TABLE `config_mod05`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `config_slide`
--
ALTER TABLE `config_slide`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `contac`
--
ALTER TABLE `contac`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
