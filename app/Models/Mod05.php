<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mod05 extends Model
{
    protected $table= 'config_mod05';
    protected $fillable = [
        'id', 'titulo', 'image',
    ];
}
