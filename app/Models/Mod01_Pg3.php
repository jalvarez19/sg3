<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mod01_Pg3 extends Model
{
    protected $table= 'config_mod01_pg3';
    protected $fillable = [
        'id', 'titulo', 'image',
    ];
}
