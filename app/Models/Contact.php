<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table= 'contac';
    protected $fillable = [
        'id', 'nombr', 'telef', 'email',
    ];
}
