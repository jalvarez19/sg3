<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Especialista extends Model
{
    protected $table= 'config_mod01_nosotros';
    protected $fillable = [
        'id', 'titulo', 'texto', 'image',
    ];
}
