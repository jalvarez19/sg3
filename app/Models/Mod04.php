<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mod04 extends Model
{
    protected $table= 'config_mod04';
    protected $fillable = [
        'id', 'titulo', 'texto',
    ];
}
