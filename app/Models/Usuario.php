<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{    
	protected $table= 'users';
    protected $fillable = [
        'id', 'name', 'email', 'category', 'na_ident',
    ];
}
