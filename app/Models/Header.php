<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Header extends Model
{
    protected $table= 'config_header';
    protected $fillable = [
        'id', 'pg1', 'pg2', 'pg3', 'pg4',
    ];
}
