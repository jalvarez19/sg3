<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $table= 'config_footer';
    protected $fillable = [
        'id', 'titulo', 'telef_f', 'telef_c',
    ];
}
