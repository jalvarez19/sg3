<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table= 'config_slide';
    protected $fillable = [
        'id', 'titulo1', 'titulo2', 'texto', 'image',
    ];
}
