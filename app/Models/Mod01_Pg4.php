<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mod01_Pg4 extends Model
{
    protected $table= 'config_mod01_pg4';
    protected $fillable = [
        'id', 'texto', 'image',
    ];
}
