<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mod03 extends Model
{
    protected $table= 'config_mod03';
    protected $fillable = [
        'id', 'titulo', 'image',
    ];
}
