<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mod02 extends Model
{
    protected $table= 'config_mod02';
    protected $fillable = [
        'id', 'titulo', 'texto', 'image',
    ];
}
