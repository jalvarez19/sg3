<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Confige;
use App\Models\Header;
use App\Models\Footer;

class Blogger extends Controller
{
    public function blog_content($id){
    	$config = Confige::first();
        $header = Header::first();
    	$blog = Blog::find($id);
    	$footer = Footer::first();
    	$blogs  = Blog::take(5)->get();
    	 return view('web/blog_conten')->with([
    	 	'blog' => $blog,
    	 	'config' => $config,
			'header' => $header,
			'footer' => $footer,
			'blogs' => $blogs,
    	 ]);
    }
}
