<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Usuario;
class Usuarios extends Controller
{
    public function index(){
    	$usuarios = Usuario::All();
    	$cont = count($usuarios);
    	return view('panel/usuarios')->with([
    		'usuarios' => $usuarios,
    		'cont' => $cont,
    	]);
    }
    public function index_register(){
    	return view('panel-usuarios/registrar');
    }
    public function active($id){
    	$usuario = Usuario::find($id);
    	$usuario->status = 'A';
    	$usuario->save();
    	return back();
    }
    public function deactivate($id){
    	$usuario = Usuario::find($id);
    	$usuario->status = 'D';
    	$usuario->save();
    	return back();
    }
    public function edit($id){
    	$usuario = Usuario::find($id);
    	return view('panel-usuarios/editar')->with([
    		'usuario' => $usuario,
    	]);
    }
    public function update(Request $request){
    	$usuario =Usuario::find($request->id);
    	$usuario->prename = $request->prename;
    	$usuario->name = $request->name;
    	$usuario->email = $request->email;
    	$usuario->na_ident = $request->na_ident;
    	$usuario->category = $request->category;
    	$usuario->adress = $request->adress;
    	if ($request->about){
    		$usuario->about = $request->about;
    	}    	
    	$usuario->save();
    	return redirect('/SG3_Usuarios');

    }
    public function register(Request $request){
    	$test = Usuario::where('email', $request->email)->first();
    	if (!$test) {
    		$usuario = new Usuario();
	    	$usuario->prename = $request->prename;
	    	$usuario->name = $request->name;
	    	$usuario->email = $request->email;
	    	$usuario->na_ident = $request->na_ident;
	    	$usuario->category = $request->category;
	    	$usuario->adress = '';
	    	$usuario->about = '';
	    	$usuario->status = 'A';
	    	$usuario->password = bcrypt($request->na_ident);
	    	$usuario->save();
	    	return redirect('/SG3_Usuarios');
    	}else{
    		return redirect('/SG3_Usuarios/Agregar');
    	}
    	
    }
}
