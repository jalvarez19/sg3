<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Confige;
class Contactos extends Controller
{
	public function index(){
		$config = Confige::first();
		$contactos = Contact::where('estado','1')->get();;
    	return view('panel/contactos')->with([
    		'config' => $config,
    		'contactos' => $contactos,
    	]);
	}
    public function registrar_contacto_trabajo(Request $request){
    	$data = $request;
    	$contact = new Contact();
    	$contact->tipo = 'Trb';
    	$contact->nombr = $data->nombre;
    	$contact->telef = $data->telefono;
    	$contact->email = $data->correo;
    	$contact->estado = '1';
        $contact->mensaje = $data->mensaje;
    	$contact->adjunto = '';
    	$contact->save();
    	return  redirect('/Inicio')->withErrors(['msg', 'The Message']);
    }
    public function registrar_contacto_empresa(Request $request){
        $data = $request;
        $contact = new Contact();
        $contact->tipo = 'Emp';
        $contact->nombr = $data->nombre;
        $contact->telef = $data->telefono;
        $contact->email = $data->correo;
        $contact->estado = '1';
        $contact->mensaje = $data->mensaje;
        $contact->adjunto = '';
        $contact->save();
        return  redirect('/Nosotros')->withErrors(['msg', 'The Message']);
    }
    public function registrar_contacto_seguridad(Request $request){
        $data = $request;
        $contact = new Contact();
        $contact->tipo = 'Emp';
        $contact->nombr = $data->nombre;
        $contact->telef = $data->telefono;
        $contact->email = $data->correo;
        $contact->estado = '1';
        $contact->mensaje = $data->mensaje;
        $contact->adjunto = '';
        $contact->save();
        return  redirect('/Seguridad')->withErrors(['msg', 'The Message']);
    }
    public function registrar_contacto_asesorias(Request $request){
        $data = $request;
        $contact = new Contact();
        $contact->tipo = 'Emp';
        $contact->nombr = $data->nombre;
        $contact->telef = $data->telefono;
        $contact->email = $data->correo;
        $contact->estado = '1';
        $contact->mensaje = $data->mensaje;
        $contact->adjunto = '';
        $contact->save();
        return  redirect('/Asesorias')->withErrors(['msg', 'The Message']);
    }
}
