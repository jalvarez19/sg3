<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Confige;
use App\Models\Header;
use App\Models\Slide;
use App\Models\Mod02;
use App\Models\Mod03;
use App\Models\Mod04;
use App\Models\Mod05;
use App\Models\Footer;
use App\Models\Especialista;
use App\Models\Mod01_Pg3;
use App\Models\Mod01_Pg4;
class Config extends Controller
{
    public function index(){
    	$config = Confige::first();
    	return view('panel-config/front-general')->with([
    		'config' => $config,
    	]);
    }
    public function index_header(){
    	$config = Confige::first();
    	$header = Header::first();
    	return view('panel-config/front-header')->with([
    		'config' => $config,
    		'header' => $header,
    	]);
    }
    public function index_footer(){
        $config = Confige::first();
        $footer = Footer::first();
        return view('panel-config/front-footer')->with([
            'config' => $config,
            'footer' => $footer,
        ]);
    }
    public function index_pg1(){
    	$config = Confige::first();
    	$header = Header::first();
    	$certificados = Mod03::where('id', '!=','1')->get();
    	$especialidades = Mod05::where('id', '!=','1')->get();
    	$mod03 = Mod03::find('1');
    	$mod04 = Mod04::first();
    	$mod05 = Mod05::find('1');

    	$slide1 = Slide::where('slide', '1')->first();
    	$slide2 = Slide::where('slide', '2')->first();
    	$slide3 = Slide::where('slide', '3')->first();

    	$prd01  = Mod02::where('prd', '1')->first();
    	$prd02  = Mod02::where('prd', '2')->first();
    	$prd03  = Mod02::where('prd', '3')->first();

    	return view('panel-config/front-inicio')->with([
    		'config' => $config,
    		'header' => $header,
    		'slide1' => $slide1,
    		'slide2' => $slide2,
    		'slide3' => $slide3,
    		'prd01'  => $prd01,
    		'prd02'  => $prd02,
    		'prd03'  => $prd03,
    		'certificados'  => $certificados,
    		'especialidades'  => $especialidades,
    		'mod03' => $mod03,
    		'mod04' => $mod04,
    		'mod05' => $mod05,
    	]);
    }
    public function index_pg2(){
        $config = Confige::first();
        $prd01  = Especialista::find(2);
        $prd02  = Especialista::find(3);
        $prd03  = Especialista::find(1);
        return view('panel-config/front-nosotros')->with([
            'config' => $config,
            'prd01'  => $prd01,
            'prd02'  => $prd02,
            'prd03'  => $prd03,
        ]);
    }
    public function index_pg3(){
        $config = Confige::first();
        $datos = Mod01_Pg3::where('id', '!=','1')->get();
        $mod01 = Mod01_Pg3::find('1');
        return view('panel-config/front-seguridad')->with([
            'config' => $config,
            'datos'  => $datos,
            'mod01' => $mod01,
        ]);
    }
    public function index_pg4(){
        $config = Confige::first();
        $datos = Mod01_Pg4::where('id', '!=','1')->get();
        $mod01 = Mod01_Pg4::find('1');
        return view('panel-config/front-asesorias')->with([
            'config' => $config,
            'datos'  => $datos,
            'mod01' => $mod01,
        ]);
    }
    public function edit(Request $request){
    	$data = $request;
    	$error = Confige::first();
    	if (!$error) {        	
        	$config = new Confige();
	        $config->site_name = $data->site_name;	    
	        $config->status = $data->status;
	        $config->descript = $data->descript;
	        $config->keywords = $data->keywords;	        
	        $config->logo_fot = '';
	        if ($data->logo) {
	        	$logo = $_FILES['logo']['name'];
		        $fotor = strtolower($logo);
		        $cd=$_FILES['logo']['tmp_name'];
		        $ruta = "img/panel/logo.png";
		        $destino = "/img/panel";
		        $resultado = @move_uploaded_file($_FILES["logo"]["tmp_name"], $ruta); 
		        $config->logo_top = $ruta;
	        }else{
	        	$config->logo_top = '';
	        }
        }else{
        	$config = Confige::first();
	        $config->site_name = $data->site_name;
	        $config->status = $data->status;
	        $config->descript = $data->descript;
	        $config->keywords = $data->keywords;	
	        if ($data->logo) {
	        	$logo = $_FILES['logo']['name'];
		        $fotor = strtolower($logo);
		        $cd=$_FILES['logo']['tmp_name'];
		        $ruta = "img/panel/logo.png";
		        $destino = "/img/panel";
		        $resultado = @move_uploaded_file($_FILES["logo"]["tmp_name"], $ruta); 
		        $config->logo_top = $ruta;
	        }
        }   	      
        $config->save();      
        return redirect('SG3_Config');
    }
    public function edit_header(Request $request){
    	$data = $request;
		$header = Header::first();
		if ($header) {
	        $header->pg1 = $data->pg1;
	        $header->pg2 = $data->pg2;
	        $header->pg3 = $data->pg3;
	        $header->subpg3_1 = $data->subpg3_1;
	        $header->subpg3_2 = $data->subpg3_2;
	        $header->pg4 = $data->pg4;
	        $header->pg5 = $data->pg5;
	        $header->save();
		}
		return redirect('SG3_Config/Header');
    }
    public function edit_footer(Request $request){
        $data = $request;
        $footer = Footer::find('1');
        $footer->titulo = $data->titulo;
        $footer->telef_f = $data->telef_f;
        $footer->telef_c = $data->telef_c;
        $footer->ubica = $data->ubica;
        $footer->fb = $data->fb;
        $footer->save();

            if ($data->logo) {
                $logo = $_FILES['logo']['name'];
                $fotor = strtolower($logo);
                $cd=$_FILES['logo']['tmp_name'];
                $ruta = "img/panel/logo_footer.png";
                $destino = "/img/panel";
                $resultado = @move_uploaded_file($_FILES["logo"]["tmp_name"], $ruta); 
                $footer->logo = $ruta;
                $config = Confige::first();
                $config->logo_fot = $ruta;
                $config->save();
             }      
            $footer->save();  
        return redirect('SG3_Config/Footer');
    }
    public function edit_pg1(Request $request){
    	$data = $request;
    	$slide1 = Slide::where('slide', '1')->first();
    	$slide2 = Slide::where('slide', '2')->first();
    	$slide3 = Slide::where('slide', '3')->first();

    	$slide1->titulo1 = $data->slide1_titulo1;
    	$slide1->titulo2 = $data->slide1_titulo2;
    	$slide1->texto = $data->slide1_texto;
    	if ($data->slide1) {
	        $slide1_img = $_FILES['slide1']['name'];
		    $fotor = strtolower($slide1_img);
		    $cd=$_FILES['slide1']['tmp_name'];
		    $ruta = "img/panel/slide1.png";
		    $destino = "/img/panel";
		    $resultado = @move_uploaded_file($_FILES["slide1"]["tmp_name"], $ruta); 
		    $slide1->image = $ruta;
	     }	    
        $slide1->save();   

        $slide2->titulo1 = $data->slide2_titulo1;
    	$slide2->titulo2 = $data->slide2_titulo2;
    	$slide2->texto = $data->slide2_texto;
    	if ($data->slide2) {
	        $slide2_img = $_FILES['slide2']['name'];
		    $fotor = strtolower($slide2_img);
		    $cd=$_FILES['slide2']['tmp_name'];
		    $ruta = "img/panel/slide2.png";
		    $destino = "/img/panel";
		    $resultado = @move_uploaded_file($_FILES["slide2"]["tmp_name"], $ruta); 
		    $slide2->image = $ruta;
	     }	    
        $slide2->save();    

        $slide3->titulo1 = $data->slide3_titulo1;
    	$slide3->titulo2 = $data->slide3_titulo2;
    	$slide3->texto = $data->slide3_texto;
    	if ($data->slide3) {
	        $slide3_img = $_FILES['slide3']['name'];
		    $fotor = strtolower($slide3_img);
		    $cd=$_FILES['slide3']['tmp_name'];
		    $ruta = "img/panel/slide3.png";
		    $destino = "/img/panel";
		    $resultado = @move_uploaded_file($_FILES["slide3"]["tmp_name"], $ruta); 
		    $slide3->image = $ruta;
	     }	    
        $slide3->save();  

        return redirect('/SG3_Config/Pg1');
    }
    public function edit_pg1_mod02(Request $request){
    	$data = $request;

    	$prd01 = Mod02::where('prd', '1')->first();
    	$prd02 = Mod02::where('prd', '2')->first();
    	$prd03 = Mod02::where('prd', '3')->first();

    	$prd03->titulo = $data->prd03_titulo;
    	$prd03->texto = $data->prd03_texto;
    	$prd03->save();
    	$prd01->titulo = $data->prd01_titulo;
    	$prd01->texto = $data->prd01_texto;
    	$prd01->link = $data->prd01_link;
    	if ($data->prd01) {
	        $prd01_img = $_FILES['prd01']['name'];
		    $fotor = strtolower($prd01_img);
		    $cd=$_FILES['prd01']['tmp_name'];
		    $ruta = "img/panel/prd01.png";
		    $destino = "/img/panel";
		    $resultado = @move_uploaded_file($_FILES["prd01"]["tmp_name"], $ruta); 
		    $prd01->image = $ruta;
	     }	    
        $prd01->save();  

        $prd02->titulo = $data->prd02_titulo;
    	$prd02->texto = $data->prd02_texto;
    	$prd02->link = $data->prd02_link;
    	if ($data->prd02) {
	        $prd02_img = $_FILES['prd02']['name'];
		    $fotor = strtolower($prd02_img);
		    $cd=$_FILES['prd02']['tmp_name'];
		    $ruta = "img/panel/prd02.png";
		    $destino = "/img/panel";
		    $resultado = @move_uploaded_file($_FILES["prd02"]["tmp_name"], $ruta); 
		    $prd02->image = $ruta;
	     }	    
        $prd02->save();  
        return redirect('/SG3_Config/Pg1'); 
    }
    public function edit_pg1_mod03(Request $request){
    	$data = $request;
    	$mod03 = Mod03::find('1');
    	$mod03->titulo = $data->mod03_titulo;
    	$mod03->save();

    	if($data->mod_03_titulo_n){
    		$mod03_n = new Mod03();
	    	$mod03_n->titulo = $data->mod_03_titulo_n;
	    	if ($data->mod_03_img_n) {
		        $mod_03_img_n = $_FILES['mod_03_img_n']['name'];
			    $fotor = strtolower($mod_03_img_n);
			    $cd=$_FILES['mod_03_img_n']['tmp_name'];
			    $ruta = "img/panel/".$fotor;
			    $destino = "/img/panel";
			    $resultado = @move_uploaded_file($_FILES["mod_03_img_n"]["tmp_name"], $ruta); 
			    $mod03_n->image = $ruta;
		     }	    
	        $mod03_n->save();  
    	}
    	

       
        return redirect('/SG3_Config/Pg1'); 
    }
    public function mod03_eliminar($id){
    	$mod03_m = Mod03::find($id);
    	$mod03_m->delete();
    	return redirect('/SG3_Config/Pg1');
    }
    public function edit_pg1_mod04(Request $request){
    	$data = $request;
    	$mod04 = Mod04::first();
    	$mod04->titulo = $data->mod04_titulo;
    	$mod04->texto = $data->mod04_texto;
    	$mod04->link = $data->mod04_link;
    	$mod04->save();
       
        return redirect('/SG3_Config/Pg1'); 
    }
    public function edit_pg1_mod05(Request $request){
    	$data = $request;
    	$mod05 = Mod05::find('1');
    	$mod05->titulo = $data->mod05_titulo;
    	$mod05->save();

    	if($data->mod_05_titulo_n){
	    	$mod05_n = new Mod05();
	    	$mod05_n->titulo = $data->mod_05_titulo_n;
	    	if ($data->mod_05_img_n) {
		        $mod_05_img_n = $_FILES['mod_05_img_n']['name'];
			    $fotor = strtolower($mod_05_img_n);
			    $cd=$_FILES['mod_05_img_n']['tmp_name'];
			    $ruta = "img/panel/".$fotor;
			    $destino = "/img/panel";
			    $resultado = @move_uploaded_file($_FILES["mod_05_img_n"]["tmp_name"], $ruta); 
			    $mod05_n->image = $ruta;
		     }	    
	        $mod05_n->save();  
	    }       
        return redirect('/SG3_Config/Pg1'); 
    }
    public function mod05_eliminar($id){
    	$mod05_m = Mod05::find($id);
    	$mod05_m->delete();
    	return redirect('/SG3_Config/Pg1');
    }
    public function edit_pg2_mod01(Request $request){
        $data = $request;

        $prd01 = Especialista::find(2);
        $prd02 = Especialista::find(3);
        $prd03 = Especialista::find(1);

        $prd03->titulo = $data->prd03_titulo;
        $prd03->texto = $data->prd03_texto;
        $prd03->head = $data->prd03_head;
        $prd03->link = '';
        $prd03->save();

        $prd01->titulo = $data->prd01_titulo;
        $prd01->texto = $data->prd01_texto;
        $prd01->link = '';
        if ($data->prd01) {
            $prd01_img = $_FILES['prd01']['name'];
            $fotor = strtolower($prd01_img);
            $cd=$_FILES['prd01']['tmp_name'];
            $ruta = "img/panel/nosotros_prd01.png";
            $destino = "/img/panel";
            $resultado = @move_uploaded_file($_FILES["prd01"]["tmp_name"], $ruta); 
            $prd01->image = $ruta;
         }      
        $prd01->save();  

        $prd02->titulo = $data->prd02_titulo;
        $prd02->texto = $data->prd02_texto;
        $prd02->link = $data->prd02_link;
        $prd02->link = '';
        if ($data->prd02) {
            $prd02_img = $_FILES['prd02']['name'];
            $fotor = strtolower($prd02_img);
            $cd=$_FILES['prd02']['tmp_name'];
            $ruta = "img/panel/nosotros_prd02.png";
            $destino = "/img/panel";
            $resultado = @move_uploaded_file($_FILES["prd02"]["tmp_name"], $ruta); 
            $prd02->image = $ruta;
         }      
        $prd02->save();  
        return redirect('/SG3_Config/Pg2'); 
    }
    public function edit_pg3_mod01(Request $request){
        $data = $request;
        $mod01 = Mod01_Pg3::find('1');
        $mod01->titulo = $data->mod01_titulo;
        $mod01->texto = $data->mod01_texto;
        $mod01->image = $data->mod01_texto2;
        $mod01->save();

        if($data->mod_01_titulo_n){
            $mod01_n = new Mod01_Pg3();
            $mod01_n->titulo = $data->mod_01_titulo_n; 
            $mod01_n->texto = '';           
            if ($data->mod_01_img_n) {
                $mod_01_img_n = $_FILES['mod_01_img_n']['name'];
                $fotor = strtolower($mod_01_img_n);
                $cd=$_FILES['mod_01_img_n']['tmp_name'];
                $ruta = "img/panel/".$fotor;
                $destino = "/img/panel/Pg3";
                $resultado = @move_uploaded_file($_FILES["mod_01_img_n"]["tmp_name"], $ruta); 
                $mod01_n->image = $ruta;
             }      
            $mod01_n->save();  
        }      
        return redirect('/SG3_Config/Pg3'); 
    }
    public function edit_pg4_mod01(Request $request){
        $data = $request;
        $mod01 = Mod01_Pg4::find('1');
        $mod01->texto = $data->mod01_titulo;
        $mod01->save();

        if($data->mod_01_titulo_n){
            $mod01_n = new Mod01_Pg4();
            $mod01_n->texto = $data->mod_01_titulo_n;         
            if ($data->mod_01_img_n) {
                $mod_01_img_n = $_FILES['mod_01_img_n']['name'];
                $fotor = strtolower($mod_01_img_n);
                $cd=$_FILES['mod_01_img_n']['tmp_name'];
                $ruta = "img/panel/".$fotor;
                $destino = "/img/panel/Pg4";
                $resultado = @move_uploaded_file($_FILES["mod_01_img_n"]["tmp_name"], $ruta); 
                $mod01_n->image = $ruta;
             }      
            $mod01_n->save();  
        }      
        return redirect('/SG3_Config/Pg4'); 
    }

}
