<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Confige;
use App\Models\Header;
use App\Models\Slide;
use App\Models\Mod02;
use App\Models\Mod03;
use App\Models\Mod04;
use App\Models\Mod05;
use App\Models\Footer;
use App\Models\Especialista;
use App\Models\Mod01_Pg3;
use App\Models\Mod01_Pg4;
use App\Models\Blog;
class Pages extends Controller
{
    public function inicio(){
        $config = Confige::first();
        $header = Header::first();
        $slide1 = Slide::where('slide', '1')->first();
        $slide2 = Slide::where('slide', '2')->first();
        $slide3 = Slide::where('slide', '3')->first();

        $certificados = Mod03::where('id', '!=','1')->get();
        $mod03 = Mod03::find('1');
        $mod04 = Mod04::first();
        $especialidades = Mod05::where('id', '!=','1')->get();
        $mod05 = Mod05::find('1');
        $footer = Footer::first();

        $prd01 = Mod02::where('prd', '1')->first();
        $prd02 = Mod02::where('prd', '2')->first();
        $prd03 = Mod02::where('prd', '3')->first();
        $blogs  = Blog::take(5)->get();

        if ($config->status == 'OFF') {
            return view('web/maintenance');
        }
    	return view('web/inicio')->with([
            'config' => $config,
            'header' => $header,
            'slide1' => $slide1,
            'slide2' => $slide2,
            'slide3' => $slide3,
            'prd01'  => $prd01,
            'prd02'  => $prd02,
            'prd03'  => $prd03,
            'certificados'  => $certificados,
            'especialidades'  => $especialidades,
            'mod03' => $mod03,
            'mod04' => $mod04,
            'mod05' => $mod05,
            'footer' => $footer,
            'blogs' => $blogs,
        ]);
    }
    public function nosotros(){
        $config = Confige::first();
        $header = Header::first();
        $footer = Footer::first();
        $prd01 = Especialista::find(2);
        $prd02 = Especialista::find(3);
        $prd03 = Especialista::find(1);
        $blogs  = Blog::take(5)->get();
        if ($config->status == 'OFF') {
            return view('web/maintenance');
        }
    	return view('web/nosotros')->with([
            'config' => $config,
            'header' => $header,
            'footer' => $footer,
            'prd01'  => $prd01,
            'prd02'  => $prd02,
            'prd03'  => $prd03,
            'blogs' => $blogs,
        ]);
    }
    public function seguridad(){
        $config = Confige::first();
        $header = Header::first();
        $footer = Footer::first();
        $datos = Mod01_Pg3::where('id', '!=','1')->get();
        $mod01 = Mod01_Pg3::find('1');
        $blogs  = Blog::take(5)->get();
        if ($config->status == 'OFF') {
            return view('web/maintenance');
        }
    	return view('web/seguridad')->with([
            'config' => $config,
            'header' => $header,
            'footer' => $footer,
            'datos' => $datos,
            'mod01' => $mod01,
            'blogs' => $blogs,
        ]);
    }
    public function asesorias(){
        $config = Confige::first();
        $header = Header::first();
        $footer = Footer::first();
        $datos = Mod01_Pg4::where('id', '!=','1')->get();
        $mod01 = Mod01_Pg4::find('1');
        $blogs  = Blog::take(5)->get();
        if ($config->status == 'OFF') {
            return view('web/maintenance');
        }
        return view('web/asesorias')->with([
            'config' => $config,
            'header' => $header,
            'footer' => $footer,
            'datos'  => $datos,
            'mod01' => $mod01,
            'blogs' => $blogs,
        ]);
    }
    public function blog(){
        $config = Confige::first();
        $header = Header::first();
        $footer = Footer::first();
        $blogs  = Blog::take(5)->get();
        foreach ($blogs as $blog) {
            $cadena = $blog->texto;
            $caracteres = 600;
            $blog->texto = substr($cadena, 0, $caracteres).'.....';
        }        

        if ($config->status == 'OFF') {
            return view('web/maintenance');
        }
    	return view('web/blog')->with([
            'config' => $config,
            'header' => $header,
            'footer' => $footer,
            'blogs' => $blogs,
        ]);
    }
    public function contacto(){
        $config = Confige::first();
        $header = Header::first();
        $footer = Footer::first();
        $blogs  = Blog::take(5)->get();
        if ($config->status == 'OFF') {
            return view('web/maintenance');
        }
    	return view('web/contacto')->with([
            'config' => $config,
            'header' => $header,
            'footer' => $footer,
            'blogs' => $blogs,
        ]);
    }
}
