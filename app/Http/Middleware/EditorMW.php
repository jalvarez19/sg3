<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class EditorMW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $category = Auth::User()->category;
        if ($category == 'Adm' || $category == 'Edt') {
            return $next($request);
        }else{
            return back();
        }
        
    }
}
