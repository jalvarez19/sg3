<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigFooterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_footer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 100);
            $table->string('telef_f', 100);
            $table->string('telef_c', 100);
            $table->string('ubica', 100);
            $table->string('fb', 100);
            $table->string('logo', 400);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_footer');
    }
}
