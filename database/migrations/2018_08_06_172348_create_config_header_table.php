<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_header', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pg1');
            $table->string('pg2');
            $table->string('pg3');
            $table->string('subpg3-1');
            $table->string('subpg3-2');
            $table->string('pg4');
            $table->string('pg5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_header');
    }
}
