<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContacTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contac', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->string('nombr', 100);
            $table->string('telef', 50);
            $table->string('email', 100);
            $table->string('mensaje', 3000);
            $table->string('adjunto', 500);
            $table->string('estado', 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contac');
    }
}
