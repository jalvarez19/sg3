<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Web\Pages@inicio')->name('Inicio');

Auth::routes();

Route::get('/Inicio', 'Web\Pages@inicio')->name('Inicio');
Route::get('/Nosotros', 'Web\Pages@nosotros')->name('Nosotros');
Route::get('/Seguridad', 'Web\Pages@seguridad')->name('Seguridad');
Route::get('/Asesorias', 'Web\Pages@asesorias')->name('Asesorias');
Route::get('/Blog', 'Web\Pages@blog')->name('Blog');
Route::get('/Blog/{id}', 'Panel\Blogger@blog_content');
Route::get('/Contacto', 'Web\Pages@contacto')->name('Contacto');

Route::post('/Formulario_Trabajo', 'Panel\Contactos@registrar_contacto_trabajo');
Route::post('/Formulario_Empresa', 'Panel\Contactos@registrar_contacto_empresa');
Route::post('/Formulario_Seguridad', 'Panel\Contactos@registrar_contacto_seguridad'); 			
Route::post('/Formulario_Asesorias', 'Panel\Contactos@registrar_contacto_asesorias'); 

//loggin
Route::group(['middleware' => ['auth']], function () {	
	//Editor
	Route::get('/SG3_Panel', 'Panel\Home@index');

	Route::group(['middleware' => ['Edt']], function () {

	  	Route::get('/SG3_Usuarios', 'Panel\Usuarios@index'); 
	  	Route::get('/SG3_Contactos', 'Panel\Contactos@index'); 		
	  	

	    	//Administrador
	    	Route::group(['middleware' => ['Adm']], function () {
		    	Route::get('/SG3_Usuarios/Activar_u/{id}', 'Panel\Usuarios@active');
	  			Route::get('/SG3_Usuarios/Desactivar_u/{id}', 'Panel\Usuarios@deactivate');
	  			Route::get('/SG3_Usuarios/Editar_u/{id}', 'Panel\Usuarios@edit');
	  			Route::get('/SG3_Usuarios/Agregar', 'Panel\Usuarios@index_register');	
	  			Route::post('/SG3_Usuarios/Agregar', 'Panel\Usuarios@register');	  			
	  			Route::post('/SG3_Usuarios/Editar_u', 'Panel\Usuarios@update');


	  			Route::get('/SG3_Config', 'Panel\Config@index'); 
	  			Route::post('/SG3_Config', 'Panel\Config@edit'); 
	  			Route::get('/SG3_Config/Header', 'Panel\Config@index_header'); 
	  			Route::post('/SG3_Config/Header', 'Panel\Config@edit_header'); 
	  			Route::get('/SG3_Config/Footer', 'Panel\Config@index_footer');
	  			Route::post('/SG3_Config/Footer', 'Panel\Config@edit_footer');
	  			Route::get('/SG3_Config/Pg1', 'Panel\Config@index_pg1'); 
	  			Route::post('/SG3_Config/Pg1', 'Panel\Config@edit_pg1'); 	
	  			Route::post('/SG3_Config/Pg1/Mod02', 'Panel\Config@edit_pg1_mod02'); 
	  			Route::post('/SG3_Config/Pg1/Mod03', 'Panel\Config@edit_pg1_mod03'); 
	  			Route::post('/SG3_Config/Pg1/Mod04', 'Panel\Config@edit_pg1_mod04'); 	
	  			Route::post('/SG3_Config/Pg1/Mod05', 'Panel\Config@edit_pg1_mod05'); 
	  			Route::get('/SG3_Config/Pg1/CertificadoEl/{id}', 'Panel\Config@mod03_eliminar');
	  			Route::get('/SG3_Config/Pg1/EspecialidadEl/{id}', 'Panel\Config@mod05_eliminar');
	  			Route::get('/SG3_Config/Pg2', 'Panel\Config@index_pg2'); 
	  			Route::post('/SG3_Config/Pg2/Mod01', 'Panel\Config@edit_pg2_mod01'); 
	  			Route::get('/SG3_Config/Pg3', 'Panel\Config@index_pg3'); 
	  			Route::post('/SG3_Config/Pg3/Mod01', 'Panel\Config@edit_pg3_mod01'); 
	  			Route::get('/SG3_Config/Pg4', 'Panel\Config@index_pg4'); 
	  			Route::post('/SG3_Config/Pg4/Mod01', 'Panel\Config@edit_pg4_mod01'); 
		    });
		    //EndAdministrador
	});
	//EndEditor   
});
//EndLoggin
